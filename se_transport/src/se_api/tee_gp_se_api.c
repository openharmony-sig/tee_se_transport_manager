/*
 * Copyright (C) 2022 Huawei Technologies Co., Ltd.
 * Licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <securec.h>
#include <stddef.h>
#include "sesrv_api.h"
#include "se_service.h"
#include "tee_defines.h"
#include "se_internal_api.h"

/*
 * -----------------------------------------------------------------------------------------------
 * APIs under the line are defined by Global Platform, need to follow Global Platform code style
 * don't change function name / return value type / parameters types / parameters names
 * -----------------------------------------------------------------------------------------------
 */
TEE_Result TEE_SEServiceOpen(TEE_SEServiceHandle *se_service_handle)
{
    return se_internal_service_open(se_service_handle);
}

void TEE_SEServiceClose(TEE_SEServiceHandle se_service_handle)
{
    se_internal_service_close(se_service_handle);
}

TEE_Result TEE_SEServiceGetReaders(TEE_SEServiceHandle se_service_handle, TEE_SEReaderHandle *se_reader_handle_list,
                                   uint32_t *se_reader_handle_list_len)
{
    return se_internal_service_get_readers(se_service_handle, se_reader_handle_list, se_reader_handle_list_len);
}

void TEE_SEReaderGetProperties(TEE_SEReaderHandle se_reader_handle, TEE_SEReaderProperties *reader_properties)
{
    se_internal_reader_get_properties(se_reader_handle, reader_properties);
}

TEE_Result TEE_SEReaderGetName(TEE_SEReaderHandle se_reader_handle, char *reader_name, uint32_t *reader_name_len)
{
    return se_internal_reader_get_name(se_reader_handle, reader_name, reader_name_len);
}

TEE_Result TEE_SEReaderOpenSession(TEE_SEReaderHandle se_reader_handle, TEE_SESessionHandle *se_session_handle)
{
    return se_internal_reader_open_session(se_reader_handle, se_session_handle);
}

void TEE_SEReaderCloseSessions(TEE_SEReaderHandle se_reader_handle)
{
    se_internal_reader_close_sessions(se_reader_handle);
}

/* atr & atr_len not implement in SE driver */
TEE_Result TEE_SESessionGetATR(TEE_SESessionHandle se_session_handle, void *atr, uint32_t *atr_len)
{
    return se_internal_session_get_atr(se_session_handle, atr, atr_len);
}

TEE_Result TEE_SESessionIsClosed(TEE_SESessionHandle se_session_handle)
{
    return se_internal_session_is_closed(se_session_handle);
}

void TEE_SESessionClose(TEE_SESessionHandle se_session_handle)
{
    se_internal_session_close(se_session_handle);
}

void TEE_SESessionCloseChannels(TEE_SESessionHandle se_session_handle)
{
    se_internal_session_close_channels(se_session_handle);
}

TEE_Result TEE_SESessionOpenBasicChannel(TEE_SESessionHandle se_session_handle, TEE_SEAID *se_aid,
                                         TEE_SEChannelHandle *se_channel_handle)
{
    return se_internal_session_open_basic_channel(se_session_handle, se_aid, se_channel_handle);
}

TEE_Result TEE_SESessionOpenLogicalChannel(TEE_SESessionHandle se_session_handle, TEE_SEAID *se_aid,
                                           TEE_SEChannelHandle *se_channel_handle)
{
    return se_internal_session_open_logical_channel(se_session_handle, se_aid, se_channel_handle);
}

void TEE_SEChannelClose(TEE_SEChannelHandle se_channel_handle)
{
    se_internal_channel_close(se_channel_handle);
}

TEE_Result TEE_SEChannelSelectNext(TEE_SEChannelHandle se_channel_handle)
{
    return se_internal_channel_select_next(se_channel_handle);
}

TEE_Result TEE_SEChannelGetSelectResponse(TEE_SEChannelHandle se_channel_handle, void *response, uint32_t *response_len)
{
    return se_internal_channel_get_select_response(se_channel_handle, response, response_len);
}

TEE_Result TEE_SEChannelTransmit(TEE_SEChannelHandle se_channel_handle, void *command, uint32_t command_len,
                                 void *response, uint32_t *response_len)
{
    return se_internal_channel_transmit(se_channel_handle, command, command_len, response, response_len);
}

TEE_Result TEE_SESecureChannelOpen(TEE_SEChannelHandle se_channel_handle, TEE_SC_Params *sc_params)
{
    return se_internal_secure_channel_open(se_channel_handle, sc_params);
}

void TEE_SESecureChannelClose(TEE_SEChannelHandle se_channel_handle)
{
    se_internal_secure_channel_close(se_channel_handle);
}

TEE_Result TEE_SEChannelGetID(TEE_SEChannelHandle se_channel_handle, uint8_t *channel_id)
{
    return se_internal_channel_get_id(se_channel_handle, channel_id);
}
