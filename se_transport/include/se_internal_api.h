/*
 * Copyright (C) 2022 Huawei Technologies Co., Ltd.
 * Licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#ifndef SE_INTERNAL_API_H
#define SE_INTERNAL_API_H

#include "tee_internal_se_api.h"

TEE_Result se_internal_service_open(TEE_SEServiceHandle *se_service_handle);
void se_internal_service_close(TEE_SEServiceHandle se_service_handle);
TEE_Result se_internal_service_get_readers(TEE_SEServiceHandle se_service_handle,
    TEE_SEReaderHandle *se_reader_handle_list,
    uint32_t *se_reader_handle_list_len);
void se_internal_reader_get_properties(TEE_SEReaderHandle se_reader_handle,
    TEE_SEReaderProperties *reader_properties);
TEE_Result se_internal_reader_get_name(TEE_SEReaderHandle se_reader_handle,
    char *reader_name, uint32_t *reader_name_len);
TEE_Result se_internal_reader_open_session(TEE_SEReaderHandle se_reader_handle,
    TEE_SESessionHandle *se_session_handle);
void se_internal_reader_close_sessions(TEE_SEReaderHandle se_reader_handle);
TEE_Result se_internal_session_get_atr(TEE_SESessionHandle se_session_handle,
    void *atr, uint32_t *atr_len);
TEE_Result se_internal_session_is_closed(TEE_SESessionHandle se_session_handle);
void se_internal_session_close(TEE_SESessionHandle se_session_handle);
void se_internal_session_close_channels(TEE_SESessionHandle se_session_handle);
TEE_Result se_internal_session_open_basic_channel(TEE_SESessionHandle se_session_handle,
    TEE_SEAID *se_aid, TEE_SEChannelHandle *se_channel_handle);
TEE_Result se_internal_session_open_logical_channel(TEE_SESessionHandle se_session_handle,
    TEE_SEAID *se_aid, TEE_SEChannelHandle *se_channel_handle);
void se_internal_channel_close(TEE_SEChannelHandle se_channel_handle);
TEE_Result se_internal_channel_select_next(TEE_SEChannelHandle se_channel_handle);
TEE_Result se_internal_channel_get_select_response(TEE_SEChannelHandle se_channel_handle,
    void *response, uint32_t *response_len);
TEE_Result se_internal_channel_transmit(TEE_SEChannelHandle se_channel_handle,
    void *command, uint32_t command_len,
    void *response, uint32_t *response_len);
TEE_Result se_internal_secure_channel_open(TEE_SEChannelHandle se_channel_handle,
    TEE_SC_Params *sc_params);
void se_internal_secure_channel_close(TEE_SEChannelHandle se_channel_handle);
TEE_Result se_internal_channel_get_id(TEE_SEChannelHandle se_channel_handle,
    uint8_t *channel_id);
#endif
